﻿using Xunit;
using System.Linq;
using System.Threading;
using System;
using System.IO;
using System.Xml.Serialization;
using XMLParsing;

namespace Integration.Tests
{
    public class IntegrationTests
    {
        static CommunicationServer.TcpServer server;
        static Client.MessageReceiver messageReceiver;
        static GameMaster.XmlParser pars;
        static GameMaster.Game game;
        static GameMaster.Communication communication;
        static RegisterGame registerGame;
        static GameInfo gameInfo;

         static void CommunicationServer(object data)
        {
            server = new CommunicationServer.TcpServer();
            server.Run(8008);
        }
         static void Client(object data)
        {
           messageReceiver = new Client.MessageReceiver("127.0.0.1", 8008);

        }
         static void GameMaster(object data)
        {
            pars = new GameMaster.XmlParser("Test.xml");
            pars.Parse();

            game = new GameMaster.Game(pars);

            game.GameInfo();

            game.DisplayGameState();

            communication = new GameMaster.Communication("127.0.0.1", 8008, game);

            registerGame = new RegisterGame();
            gameInfo = new GameInfo();
            gameInfo.gameName = game._gameName;
            gameInfo.redTeamPlayers = Convert.ToUInt64(game._redPlayersCount);
            gameInfo.blueTeamPlayers = Convert.ToUInt64(game._bluePlayersCount);

            registerGame.NewGameInfo = gameInfo;
            communication.SendMessage(registerGame);
        }
        [Fact]
        public void staringTest()
        {
            bool result = true;
            Thread serverThread = new Thread(CommunicationServer);
            Thread clientThread = new Thread(Client);
            Thread gmThread = new Thread(GameMaster);
            serverThread.Start();
            clientThread.Start();
            gmThread.Start();


            Assert.True(result);
        }
        [Fact]
        public void GameWinningTest()
        {
            Thread serverThread = new Thread(CommunicationServer);
            Thread clientThread = new Thread(Client);
            Thread gmThread = new Thread(GameMaster);

            serverThread.Start();
            clientThread.Start();
            gmThread.Start();
            serverThread.Join();
            clientThread.Join();
            gmThread.Join();

            bool result = game.won;
            Assert.True(result);
        }
        [Fact]
        public void PlayersLeaderTest()
        {
            Thread serverThread = new Thread(CommunicationServer);
            Thread clientThread = new Thread(Client);
            Thread gmThread = new Thread(GameMaster);
            serverThread.Start();
            clientThread.Start();
            gmThread.Start();
            serverThread.Join();
            clientThread.Join();
            gmThread.Join();

            bool result = game._blueHaveLeader;
            Assert.True(game._blueHaveLeader);
        }
        [Fact]
        public void PlayersFromClientTest()
        {
            Thread serverThread = new Thread(CommunicationServer);
            Thread clientThread = new Thread(Client);
            Thread gmThread = new Thread(GameMaster);
            serverThread.Start();
            clientThread.Start();
            gmThread.Start();
            serverThread.Join();
            clientThread.Join();
            gmThread.Join();

            bool result = true;
            if (game._bluePlayers.Count != game._bluePlayersCount)
                result = false;
            Assert.True(result);
        }
        [Fact]
        public void PlayersFromClientNonZeroTest()
        {
            Thread serverThread = new Thread(CommunicationServer);
            Thread clientThread = new Thread(Client);
            Thread gmThread = new Thread(GameMaster);
            serverThread.Start();
            clientThread.Start();
            gmThread.Start();
            serverThread.Join();
            clientThread.Join();
            gmThread.Join();

            bool result = true;
            if (game._bluePlayers.Count ==0)
                result = false;
            Assert.True(result);
        }
        /*[Fact]
        public void someTest()
        {

            Thread serverThread = new Thread(CommunicationServer);
            Thread clientThread = new Thread(Client);
            Thread gmThread = new Thread(GameMaster);
            serverThread.Start();
            clientThread.Start();
            gmThread.Start();
            serverThread.Join();
            clientThread.Join();
            gmThread.Join();

            
            
            bool result = true;
            Assert.True(result);
        }*/
    }
}
