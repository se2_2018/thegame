﻿namespace Client
{
    public class Location
    {
        public int x;
        public int y;

        public Location(int _x, int _y)
        {
            x = _x;
            y = _y;
        }

        public static bool operator ==(Location l1, Location l2)
        {
            if (ReferenceEquals(l1, null))
            {
                return ReferenceEquals(l2, null);
            }

            return l1.Equals(l2);
        }

        public static bool operator !=(Location l1, Location l2)
        {
            return !(l1 == l2);
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            var l2 = (Location) obj;
            return x == l2.x && y == l2.y;
        }
    }

    public class Piece
    {
        public Piece(Location _location, int _Id, bool _IsSham)
        {
            Location = _location;
            Id = _Id;
            IsSham = _IsSham;
        }

        public int Id;
        public Location Location;
        public bool IsSham;
    }
}