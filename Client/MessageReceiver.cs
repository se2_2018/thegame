﻿using System;
using System.Collections.Generic;
using System.Text;
using XMLParsing;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using System.Net.Sockets;
using System.Threading;
using System.Xml.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Xml;

namespace Client
{
    public class MessageReceiver
    {
        Player _player;

        //TODO: Fix communication and receiving messages


        NetworkStream _networkStream;
        TcpClient _tcpClient;
        private string _hostName;
        private int _portNumber;

        public MessageReceiver(string hostName, int portNumber)
        {
            _hostName = hostName;
            _portNumber = portNumber;
            Connect();
            _player = new Player(_tcpClient);
        }

        public void Connect()
        {
            _tcpClient = new TcpClient(_hostName, _portNumber);

            if (_tcpClient.Connected == false)
            {
                Console.WriteLine(@"Connection refused!");
                return;
            }

            //_tcpClient.Connect("localhost", 8008);
            Thread _tcpMessageReceiverThread = new Thread(ReceiveMessage);
            _tcpMessageReceiverThread.Start();
        }

        private void ReceiveMessage()
        {
            _networkStream = _tcpClient.GetStream();
            byte[] bytes = new byte[4096];

            while (true)
            {
                if (_networkStream.DataAvailable)
                {
                    int received;
                    received = _networkStream.Read(bytes, 0, bytes.Length);
                    string xmlMessage = Encoding.UTF8.GetString(bytes, 0, received);
                    Console.Write(@"New message received (" + xmlMessage + "): ");
                    ParseMessage(xmlMessage);
                }
            }
        }

        public void ParseMessage(string xmlMessage)
        {
            var singleMessages = Regex.Split(xmlMessage, @"(?=<\?xml version)");
            singleMessages = singleMessages.Where(x => !string.IsNullOrEmpty(x)).ToArray();

            foreach (var singleMessage in singleMessages)
            {
                XmlDocument doc = new XmlDocument();
                try
                {
                    doc.LoadXml(singleMessage);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Cannot parse {0}", singleMessage);
                    Console.WriteLine(e.ToString());
                    break;
                }
                string messageType = doc.DocumentElement.Name;
                switch (messageType)
                {
                    case "ConfirmGameRegistration":
                        ReceiveConfirmGameRegistration(new MemoryStream(Encoding.UTF8.GetBytes(xmlMessage)));
                        break;
                    case "RejectGameRegistration":
                        ReceiveRejectGameRegistration(new MemoryStream(Encoding.UTF8.GetBytes(xmlMessage)));
                        break;
                    case "RegisteredGames":
                        ReceiveRegisteredGames(new MemoryStream(Encoding.UTF8.GetBytes(xmlMessage)));
                        break;
                    case "ConfirmJoiningGame":
                        ReceiveConfirmJoining(new MemoryStream(Encoding.UTF8.GetBytes(xmlMessage)));
                        break;
                    case "RejectJoiningGame":
                        ReceiveRejectJoining(new MemoryStream(Encoding.UTF8.GetBytes(xmlMessage)));
                        break;
                    case "RejectKnowledgeExchange":
                        ReceiveRejectKnowledgeExchange(new MemoryStream(Encoding.UTF8.GetBytes(xmlMessage)));
                        break;
                    case "<KnowledgeExchangeRequest":
                        ReceiveKnowledgeExchangeRequest(new MemoryStream(Encoding.UTF8.GetBytes(xmlMessage)));
                        break;
                    case "AcceptExchangeRequest":
                        ReceiveAcceptKnowledgeExchange(new MemoryStream(Encoding.UTF8.GetBytes(xmlMessage)));
                        break;
                    case "Game":
                        ReceiveGame(new MemoryStream(Encoding.UTF8.GetBytes(xmlMessage)));
                        break;
                    case "Data":
                        ReceiveData(new MemoryStream(Encoding.UTF8.GetBytes(xmlMessage)));
                        break;
                }
            }
        }


        public void ReceiveConfirmGameRegistration(Stream message)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(ConfirmGameRegistration));
            ConfirmGameRegistration registerGame = serializer.Deserialize(message) as ConfirmGameRegistration;
            if (registerGame != null)
            {
                Console.WriteLine("Game registered");
                _player.GetGames();
            }
            else
            {
                throw new Exception("Error serializing ConfirmGameRegistration.xml");
            }
        }

        public void ReceiveRejectGameRegistration(Stream message)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(RejectGameRegistration));
            RejectGameRegistration rejectGameRegistration = serializer.Deserialize(message) as RejectGameRegistration;
            if (rejectGameRegistration != null)
            {
                Console.WriteLine("Rejected to register:");
                Console.WriteLine(rejectGameRegistration.gameName);
            }
            else
            {
                throw new Exception("Error serializing RejectGameRegistration.xml");
            }
        }

        public void ReceiveRegisteredGames(Stream message)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(RegisteredGames));
            RegisteredGames registeredGames = serializer.Deserialize(message) as RegisteredGames;
            if (registeredGames != null)
            {
                Console.WriteLine("Game registered:");
                GameInfo[] games = registeredGames.GameInfo;
                if (games.Length == 0)
                {
                    _player.RegisterGame();
                }
                else
                {
                    for (int i = 0; i < games.Length; i++)
                    {
                        Console.WriteLine("Number:");
                        Console.WriteLine((i + 1).ToString());
                        Console.WriteLine("Name:");
                        Console.WriteLine(games[i].gameName);
                        Console.WriteLine("Number of blue players:");
                        Console.WriteLine(games[i].blueTeamPlayers.ToString());
                        Console.WriteLine("Number of red players:");
                        Console.WriteLine(games[i].redTeamPlayers.ToString());
                        i++;
                    }

                    Console.WriteLine("Choose the game (type number) or create new game (type '0')");
                    int choice = Int32.Parse(Console.ReadLine());
                    if (choice == 0)
                        _player.RegisterGame();
                    else
                    {
                        string name = games[(choice - 1)].gameName;
                        Console.WriteLine("Preffered role - leader (type '0'), member (type '1')");
                        XMLParsing.PlayerRole role;
                        if (Int32.Parse(Console.ReadLine()) == 0)
                        {
                            role = PlayerRole.leader;
                        }
                        else
                        {
                            role = PlayerRole.member;
                        }

                        Console.WriteLine("preferedTeam - red(type '0'), blue (type '1')");
                        XMLParsing.TeamColour team;
                        if (Int32.Parse(Console.ReadLine()) == 0)
                        {
                            team = TeamColour.red;
                        }
                        else
                        {
                            team = TeamColour.blue;
                        }

                        _player.JoinGame(name, role, team);
                    }
                }
            }
            else
            {
                throw new Exception("Error serializing RegisteredGames.xml");
            }
        }

        public void ReceiveConfirmJoining(Stream message)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(ConfirmJoiningGame));
            ConfirmJoiningGame confirmJoiningGame = serializer.Deserialize(message) as ConfirmJoiningGame;
            if (confirmJoiningGame != null)
            {
                Console.WriteLine("Joined");
                _player.Initialize(Convert.ToInt32(confirmJoiningGame.gameId),
                    Convert.ToInt32(confirmJoiningGame.playerId), confirmJoiningGame.privateGuid,
                    confirmJoiningGame.PlayerDefinition.team, confirmJoiningGame.PlayerDefinition.role);
            }
            else
            {
                throw new Exception("Error serializing ConfirmJoining.xml");
            }
        }

        public void ReceiveRejectJoining(Stream message)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(RejectJoiningGame));
            RejectJoiningGame rejectJoiningGame = serializer.Deserialize(message) as RejectJoiningGame;
            if (rejectJoiningGame != null)
            {
                Console.WriteLine("Rejected to join to:");
                Console.WriteLine(rejectJoiningGame.gameName);
            }
            else
            {
                throw new Exception("Error serializing RejectJoining.xml");
            }
        }

        public void ReceiveRejectKnowledgeExchange(Stream message)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(RejectKnowledgeExchange));
            RejectKnowledgeExchange rejectKnowledgeExchange =
                serializer.Deserialize(message) as RejectKnowledgeExchange;
            if (rejectKnowledgeExchange != null)
            {
                Console.WriteLine("Rejected to exchange knowledge with:");
                Console.WriteLine(rejectKnowledgeExchange.senderPlayerId.ToString());
            }
            else
            {
                throw new Exception("Error serializing RejectKnowledgeExchange.xml");
            }
        }

        public void ReceiveAcceptKnowledgeExchange(Stream message)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(AcceptExchangeRequest));
            AcceptExchangeRequest acceptExchangeRequest = serializer.Deserialize(message) as AcceptExchangeRequest;
            if (acceptExchangeRequest != null)
            {
                Console.WriteLine("Accepted to exchange knowledge with:");
                Console.WriteLine(acceptExchangeRequest.senderPlayerId.ToString());
            }
            else
            {
                throw new Exception("Error serializing AcceptExchangeRequest.xml");
            }
        }

        public void ReceiveKnowledgeExchangeRequest(Stream message)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(KnowledgeExchangeRequest));
            KnowledgeExchangeRequest knowledgeExchangeRequest = serializer.Deserialize(message) as KnowledgeExchangeRequest;
            if (knowledgeExchangeRequest != null && Convert.ToInt32(knowledgeExchangeRequest.playerId) == _player.PlayerId)
            {
                _player.SendKnowledge(Convert.ToInt32(knowledgeExchangeRequest.senderPlayerId));
                Console.WriteLine("Got a request to exchange knowledge");
            }
            else
            {
                throw new Exception("Error serializing KnowledgeExchangeRequest.xml");
            }
        }

        public void ReceiveGame(Stream message)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Game));
            Game game = serializer.Deserialize(message) as Game;
            if (game != null)
            {
                GameBoard gameBoard = game.Board;
                Location location = new Location(Convert.ToInt32(game.PlayerLocation.x),
                    Convert.ToInt32(game.PlayerLocation.y));
                _player.StartGame(Convert.ToInt32(gameBoard.width), Convert.ToInt32(gameBoard.tasksHeight),
                    Convert.ToInt32(gameBoard.goalsHeight), location, game.Players);
            }
            else
            {
                throw new Exception("Error serializing Game.xml");
            }
        }

        public void ReceiveData(Stream message)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Data));
            Data data = serializer.Deserialize(message) as Data;
            if (data != null)
            {
                if (data.PlayerLocation != null)
                {
                    _player.UpdateLoaction(data.PlayerLocation);
                }

                if (data.Pieces != null)
                {
                    _player.UpdatePieces(data.Pieces);
                }

                if (data.GoalFields != null)
                {
                    _player.UpdateGoals(data.GoalFields);
                }

                if (data.TaskFields != null)
                {
                    _player.UpdateTasks(data.TaskFields);
                }
            }
            else
            {
                throw new Exception("Error serializing Data.xml");
            }
        }
    }
}