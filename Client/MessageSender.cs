﻿using System;
using System.Collections.Generic;
using System.Text;
using XMLParsing;
using System.IO;
using System.Xml.Serialization;
using System.Net.Sockets;
using System.Threading;
using System.Xml.Linq;
namespace Client
{
    public class MessageSender
    {
        Player _player;
        NetworkStream _networkStream;
        TcpClient _tcpClient;
        public MessageSender(Player player, TcpClient client)
        {
            _tcpClient = client;
            _player = player;
        }

        public void SendGetGames()
        {
            GetGames getGames = new GetGames();
            XmlSerializer serializer = new XmlSerializer(typeof(GetGames));
            var memoryStream = new MemoryStream();
            using (var streamWriter = new StreamWriter(memoryStream, new UTF8Encoding(false)))
            {

                serializer.Serialize(streamWriter, getGames);
                SendMessageFromText(memoryStream);
            }
        }

        public void SendRegisterGame(string name, int NumberOfBluePlayers, int NumberOfRedPlayers)
        {
            RegisterGame registerGame = new RegisterGame();
            GameInfo gameInfo = new GameInfo();
            gameInfo.gameName = name;
            gameInfo.blueTeamPlayers = Convert.ToUInt64(NumberOfBluePlayers);
            gameInfo.redTeamPlayers = Convert.ToUInt64(NumberOfRedPlayers);
            registerGame.NewGameInfo = gameInfo;
            XmlSerializer serializer = new XmlSerializer(typeof(RegisterGame));
            var memoryStream = new MemoryStream();
            using (var textWriter = new StreamWriter(memoryStream, new UTF8Encoding(false)))
            {
                serializer.Serialize(textWriter, registerGame);
                SendMessageFromText(memoryStream);
            }
        }

        public void SendJoinGame(string name, PlayerRole role, TeamColour team)
        {
            JoinGame joinGame = new JoinGame();
            joinGame.gameName = name;
            joinGame.preferredRole = role;
            joinGame.preferredTeam = team;
            XmlSerializer serializer = new XmlSerializer(typeof(JoinGame));
            var memoryStream = new MemoryStream();
            using (var textWriter = new StreamWriter(memoryStream, new UTF8Encoding(false)))
            {
                serializer.Serialize(textWriter, joinGame);
                SendMessageFromText(memoryStream);
            }

        }

        public void SendMove(MoveType type)
        {
            Move move = new Move();
            move.direction = type;
            move.directionSpecified = true;
            move.gameId = Convert.ToUInt64(_player.GameId);
            move.playerGuid = _player.PlayerGuId;
            XmlSerializer serializer = new XmlSerializer(typeof(Move));
            var memoryStream = new MemoryStream();
            using (var textWriter = new StreamWriter(memoryStream, new UTF8Encoding(false)))
            {
                serializer.Serialize(textWriter, move);
                SendMessageFromText(memoryStream);
            }
        }


        public void SendDiscover()
        {
            Discover discover = new Discover();
            discover.gameId = Convert.ToUInt64(_player.GameId);
            discover.playerGuid = _player.PlayerGuId;
            XmlSerializer serializer = new XmlSerializer(typeof(Discover));
            var memoryStream = new MemoryStream();
            using (var textWriter = new StreamWriter(memoryStream, new UTF8Encoding(false)))
            {
                serializer.Serialize(textWriter, discover);
                SendMessageFromText(memoryStream);
            }
        }

        public void SendPickUp()
        {
            PickUpPiece pickUpPiece = new PickUpPiece();
            pickUpPiece.gameId = Convert.ToUInt64(_player.GameId);
            pickUpPiece.playerGuid = _player.PlayerGuId;
            XmlSerializer serializer = new XmlSerializer(typeof(PickUpPiece));
            var memoryStream = new MemoryStream();
            using (var textWriter = new StreamWriter(memoryStream, new UTF8Encoding(false)))
            {
                serializer.Serialize(textWriter, pickUpPiece);
                SendMessageFromText(memoryStream);
            }

        }

        public void SendTestPiece()
        {
            TestPiece testPiece = new TestPiece();
            testPiece.playerGuid = _player.PlayerGuId;
            testPiece.gameId = Convert.ToUInt64(_player.GameId);
            XmlSerializer serializer = new XmlSerializer(typeof(TestPiece));
            var memoryStream = new MemoryStream();
            using (var textWriter = new StreamWriter(memoryStream, new UTF8Encoding(false)))
            {
                serializer.Serialize(textWriter, testPiece);
                SendMessageFromText(memoryStream);
            }

        }

        public void SendPlacePiece()
        {
            PlacePiece placePiece = new PlacePiece();
            placePiece.gameId = Convert.ToUInt64(_player.GameId);
            placePiece.playerGuid = _player.PlayerGuId;
            XmlSerializer serializer = new XmlSerializer(typeof(PlacePiece));
            var memoryStream = new MemoryStream();
            using (var textWriter = new StreamWriter(memoryStream, new UTF8Encoding(false)))
            {
                serializer.Serialize(textWriter, placePiece);
                SendMessageFromText(memoryStream);
            }

        }

        public void SendAuthorizeKnowledgeExchange(int id)
        {
            AuthorizeKnowledgeExchange authorize = new AuthorizeKnowledgeExchange();
            authorize.gameId = Convert.ToUInt64(_player.GameId);
            authorize.playerGuid = _player.PlayerGuId;
            authorize.withPlayerId = Convert.ToUInt64(id);
            XmlSerializer serializer = new XmlSerializer(typeof(AuthorizeKnowledgeExchange));
            var memoryStream = new MemoryStream();
            using (var textWriter = new StreamWriter(memoryStream, new UTF8Encoding(false)))
            {
                serializer.Serialize(textWriter, authorize);
                SendMessageFromText(memoryStream);
            }
        }

        public void SendRejectKnowledgeExchange(int id)
        {
            RejectKnowledgeExchange reject = new RejectKnowledgeExchange();
            reject.permanent = false;
            reject.playerId = Convert.ToUInt64(id);
            reject.senderPlayerId = Convert.ToUInt64(_player.PlayerId);
            XmlSerializer serializer = new XmlSerializer(typeof(RejectKnowledgeExchange));
            var memoryStream = new MemoryStream();
            using (var textWriter = new StreamWriter(memoryStream, System.Text.Encoding.UTF8))
            {
                serializer.Serialize(textWriter, reject);
                SendMessageFromText(memoryStream);
            }
        }

        public void SendData(int id, XMLParsing.GoalField[] goals, XMLParsing.TaskField[] tasks, XMLParsing.Piece[] pieces)
        {
            Data data = new Data();
            data.TaskFields = tasks;
            data.Pieces = pieces;
            data.GoalFields = goals;
            data.gameFinished = false;
            data.playerId = Convert.ToUInt64(id);
            XmlSerializer serializer = new XmlSerializer(typeof(Data));
            var memoryStream = new MemoryStream();
            using (var textWriter = new StreamWriter(memoryStream, System.Text.Encoding.UTF8))
            {
                serializer.Serialize(textWriter, data);
                SendMessageFromText(memoryStream);
            }
        }


        public void SendMessageFromText(MemoryStream textWriter)
        {
            _networkStream = _tcpClient.GetStream();
            byte[] message = textWriter.ToArray();
            _networkStream.Write(message, 0, message.Length);
            Console.WriteLine(Encoding.UTF8.GetString(message));
        }
    }
}
