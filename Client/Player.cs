﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Threading;

namespace Client

{
    public class Player
    {

        public XMLParsing.TeamColour Team { get; set; }
        public XMLParsing.PlayerRole Role { get; set; }
        public string PlayerGuId { get; set; }
        public int PlayerId { get; set; }
        public int GameId;
        public int BoardWidth;
        public int TaskHeight;
        public int GoalHeight;
        public int BoardHeight;
        public bool HasPiece;
        public int lastRandom;
        public MessageSender sender;
        public Location Location;
        public List<Location> ShamPieces = new List<Location>();
        public List<XMLParsing.GoalField> Goals = new List<XMLParsing.GoalField>();
        public List<XMLParsing.TaskField> Tasks = new List<XMLParsing.TaskField>();
        public List<XMLParsing.Piece> Pieces = new List<XMLParsing.Piece>();
        public List<XMLParsing.Player> Players = new List<XMLParsing.Player>();
        public XMLParsing.TaskField Target;
        public int[,] CheckedGoalLocations;

        public Player(TcpClient client)
        {
            lastRandom = -1;
            HasPiece = false;
            sender = new MessageSender(this, client);
            GetGames();
        }

        public void RegisterGame()
        {
            Console.WriteLine("To register a game please give:");
            Console.WriteLine("Name");
            string name = Console.ReadLine();
            Console.WriteLine("blueTeamPlayers");
            int blueplayers = Int32.Parse(Console.ReadLine());
            Console.WriteLine("redTeamPlayers");
            int redplayers = Int32.Parse(Console.ReadLine());
            sender.SendRegisterGame(name, blueplayers, redplayers);
        }
        public void GetGames()
        {
            sender.SendGetGames();
        }

        public void JoinGame(string name, XMLParsing.PlayerRole role, XMLParsing.TeamColour team)
        {
            sender.SendJoinGame(name, role, team);
        }
        public void Initialize(int gameId, int playerId, string guid, XMLParsing.TeamColour team, XMLParsing.PlayerRole role)
        {
            GameId = gameId;
            PlayerId = playerId;
            PlayerGuId = guid;
            Team = team;
            Role = role;
        }

        public void StartGame(int width, int taskHeight, int goalHeight, Location playerLocaton, XMLParsing.Player[] players)
        {
            Players.AddRange(players);
            Location = playerLocaton;
            CheckedGoalLocations = new int[width, goalHeight];
            BoardWidth = width;
            TaskHeight = taskHeight;
            GoalHeight = goalHeight;
            BoardHeight = GoalHeight + GoalHeight + TaskHeight;
            PlayerWork();
        }

        private void RequestExchangeKnowledge()
        {
            List<XMLParsing.Player> allies = Players.Where(x => x.team == Team).ToList();
            int ourFriend = Convert.ToInt32(allies.First().id);
            sender.SendAuthorizeKnowledgeExchange(ourFriend);
            SendKnowledge(ourFriend);
        }

        public void SendKnowledge(int id)
        {
            sender.SendData(id, Goals.ToArray(), Tasks.ToArray(), Pieces.ToArray());

        }

        private void PlayerWork()
        {
            sender.SendDiscover();
        }

        public void PlayerInfo()
        {
            Console.WriteLine(@"Preffered team: " + Team);
            Console.WriteLine(@"Preffered role: " + Role);
            Console.WriteLine(@"Private guid: " + PlayerGuId);
        }


        public void MoveUp()
        {
            sender.SendMove(XMLParsing.MoveType.up);
        }

        public void MoveDown()
        {
            sender.SendMove(XMLParsing.MoveType.down);
        }

        public void MoveLeft()
        {
            sender.SendMove(XMLParsing.MoveType.left);
        }

        public void MoveRight()
        {
            sender.SendMove(XMLParsing.MoveType.right);
        }


        public void Discover()
        {
            sender.SendDiscover();
        }


        //Before using this function discover piece 
        public void Pick()
        {
            sender.SendPickUp();
        }

        public void Test()
        {
            sender.SendTestPiece();
        }

        public void Place()
        {
            sender.SendPlacePiece();
        }


        public void MoveToTheClosestUncheckedField()
        {
            Location closest;
            if (Team == XMLParsing.TeamColour.red)
                closest = FindTheClosestUncheckedField_RED();
            else
                closest = FindTheClosestUncheckedField_BLUE();
            if (closest == Location && HasPiece)
            {
                sender.SendPlacePiece();
            }
            if (closest.y < Location.y)
            {
                //Move up
                if (Location.y - 1 >= 0)
                {
                    MoveUp();
                }
            }
            else if (closest.y > Location.y)
            {
                //Move down
                if (Location.y + 1 < BoardHeight)
                {
                    MoveDown();
                }
            }
            else if (closest.x < Location.x)
            {
                //Move left
                if (Location.x - 1 >= 0)
                {
                    MoveLeft();
                }
            }
            else if (closest.x > Location.x)
            {
                //Move right
                if (Location.x + 1 < BoardWidth)
                {
                    MoveRight();
                }
            }
        }

        public Location FindTheClosestUncheckedField_RED()
        {
            int distance = BoardWidth * BoardHeight;
            Location loc = Location;
            for (int i = 0; i < CheckedGoalLocations.GetLength(0); i++)
            {
                for (int j = 0; j < CheckedGoalLocations.GetLength(1); j++)
                {
                    if (CheckedGoalLocations[i, j] == 1)
                    {

                    }
                    else
                    {
                        int d = Math.Abs(Location.x - i) + Math.Abs(Location.y - j);
                        if (d < distance)
                        {
                            distance = d;
                            loc.x = i;
                            loc.y = j;
                        }
                    }
                }
            }

            return loc;
        }

        public Location FindTheClosestUncheckedField_BLUE()
        {
            int distance = BoardWidth * BoardHeight; ;
            int correction = GoalHeight + TaskHeight - 1;
            Location loc = Location;
            for (int i = 0; i < CheckedGoalLocations.GetLength(0); i++)
            {
                for (int j = 0; j < CheckedGoalLocations.GetLength(1); j++)
                {
                    if (CheckedGoalLocations[i, j] == 1)
                    {
                    }
                    else
                    {
                        int d = Math.Abs(Location.x - i) + Math.Abs(Location.y - (j + correction));
                        if (d < distance)
                        {
                            distance = d;
                            loc.x = i;
                            loc.y = j + correction;
                        }
                    }
                }
            }

            return loc;
        }

        public void UpdateLoaction(XMLParsing.Location loc)
        {
            Location previous = new Location(Location.x,Location.y);
            Location.x = Convert.ToInt32(loc.x);
            Location.y = Convert.ToInt32(loc.y);
            if (Target != null)
            {
                if (Location.x == Target.x && Location.y == Target.y)
                {
                    Pick();
                    Tasks.Clear();
                    Target = null;
                    return;
                }
            }
            if (previous.x == Location.x && previous.y == Location.y)
            {
                MoveRandom();
            }
            else
            {
                if (HasPiece)
                {
                    MoveToTheClosestUncheckedField();
                }
                if (!HasPiece)
                {
                    Discover();
                }
            }

        }

        public void UpdatePieces(XMLParsing.Piece[] pieces)
        {

            for (int i = 0; i < pieces.Length; i++)
            {
                if (!Pieces.Contains(pieces[i]))
                {
                    Pieces.Add(pieces[i]);
                    if (pieces[i].type == XMLParsing.PieceType.unknown && Convert.ToInt32(pieces[i].playerId) == PlayerId)
                    {
                        sender.SendTestPiece();
                    }
                    else if (pieces[i].type == XMLParsing.PieceType.sham && Convert.ToInt32(pieces[i].playerId) == PlayerId)
                    {
                        sender.SendPlacePiece();
                        ShamPieces.Add(Location);
                        Discover();
                    }
                    else if (pieces[i].type == XMLParsing.PieceType.normal && Convert.ToInt32(pieces[i].playerId) == PlayerId)
                    {
                        HasPiece = true;
                        MoveToTheClosestUncheckedField();
                    }
                }

            }
        }

        public void UpdateCheckedGoalLocations()
        {
            if (Team == XMLParsing.TeamColour.red)
            {
                CheckedGoalLocations[Location.x, Location.y] = 1;
            }
            else
            {
                int correction = TaskHeight + GoalHeight;
                CheckedGoalLocations[Location.x, Location.y - correction] = 1;
            }
        }
        public void UpdateGoals(XMLParsing.GoalField[] goals)
        {
            for (int i = 0; i < goals.Length; i++)
            {
                //Add goals that are for our team and are not taken
                if (!Goals.Contains(goals[i]) && goals[i].playerIdSpecified == false && goals[i].team == Team)
                {
                    Goals.Add(goals[i]);
                }
                if (Convert.ToInt32(goals[i].playerId) == PlayerId && goals[i].type == XMLParsing.GoalFieldType.nongoal)
                {
                    UpdateCheckedGoalLocations();
                    MoveToTheClosestUncheckedField();
                }
                if (Convert.ToInt32(goals[i].playerId) == PlayerId && goals[i].type == XMLParsing.GoalFieldType.goal)
                {
                    HasPiece = false;
                    Discover();
                }
            }
        }
        public void UpdateTasks(XMLParsing.TaskField[] tasks)
        {

            for (int i = 0; i < tasks.Length; i++)
            {
                if (tasks[i].playerIdSpecified == false)
                    Tasks.Add(tasks[i]);
            }
            if (Target == null)
                Target = Tasks.First();
            Tasks = Tasks.OrderBy(x => x.distanceToPiece).ToList();
            Location loc = new Location(Convert.ToInt32(Tasks.First().x), Convert.ToInt32(Tasks.First().y));
            if(Tasks.First().distanceToPiece < Target.distanceToPiece)
            {
                Target = Tasks.First();
            }
            //Go to the nearest 
            if (Target.x < Location.x)
                MoveLeft();
            else if (Target.x > Location.x)
                MoveRight();
            else if (Target.y < Location.y)
                MoveUp();
            else if (Target.y > Location.y)
                MoveDown();
            else if (Tasks.First().distanceToPiece == 0) ;
            //    Pick();
            //Tasks.Clear();
        }


        public void MoveRandom()
        {
            Random r = new Random();
            int score = r.Next(1, 4);
            switch (score)
            {
                case 1 when (lastRandom != 1 && Location.y + 1 < BoardHeight):
                    lastRandom = 1;
                    MoveDown();
                    return;

                case 1 when lastRandom != 1:
                    lastRandom = 1;
                    MoveUp();
                    return;
                case 2 when (lastRandom != 2 && Location.y - 1 >= 0):
                    MoveUp();
                    lastRandom = 2;
                    return;
                case 2 when lastRandom != 2:
                    MoveDown();
                    lastRandom = 2;
                    return;
                case 3 when (lastRandom != 3 && Location.x - 1 >= 0):
                    MoveLeft();
                    lastRandom = 3;
                    return;
                case 3 when lastRandom != 3:
                    MoveRight();
                    lastRandom = 3;
                    return;
                case 4 when (lastRandom != 4 && Location.x + 1 < BoardWidth):
                    MoveRight();
                    lastRandom = 4;
                    return;
                case 4 when lastRandom != 4:
                    MoveLeft();
                    lastRandom = 4;
                    return;
                default: //if none of the above is chosen, then random move is attempted again
                    MoveRandom();
                    return;
            }
        }
    }
}