﻿using System.Collections.Generic;
using System.Net.Sockets;
using XMLParsing;

namespace CommunicationServer
{
    public class GameDTO
    {
        public List<PlayerDTO> Players = new List<PlayerDTO>();
        public GameInfo GameInfoField { get; }
        public ulong id { get; }
        public NetworkStream GameMaster { get; }

        public GameDTO(NetworkStream gameMaster, GameInfo gameInfo, ulong id)
        {
            GameMaster = gameMaster;
            GameInfoField = gameInfo;
            this.id = id;
        }
    }

    public class JoinQueue
    {
        public NetworkStream PlayerStream { get; }
        public string GameName { get; }

        public JoinQueue(NetworkStream playerStream, string gameName)
        {
            PlayerStream = playerStream;
            GameName = gameName;
        }
    }

    public class PlayerDTO
    {
        public ulong PlayerIdField;
        public PlayerRole PlayerRole;
        public TeamColour PlayerTeamColour;
        public NetworkStream PlayerSocket;

        public PlayerDTO(ulong playerIdField, PlayerRole playerRole, TeamColour playerTeamColour, NetworkStream playerSocket)
        {
            PlayerIdField = playerIdField;
            PlayerRole = playerRole;
            PlayerSocket = playerSocket;
            PlayerTeamColour = playerTeamColour;
        }
    }
}