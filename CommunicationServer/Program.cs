﻿namespace CommunicationServer
{
    class Program
    {
        static void Main(string[] args)
        {
            TcpServer server = new TcpServer();
            server.Run(8008);
        }
    }
}
//<?xml version="1.0" encoding="utf-8"?> <RegisterGame xmlns="https://se2.mini.pw.edu.pl/17-results/"> <NewGameInfo gameName="easyGame" blueTeamPlayers="2" redTeamPlayers="2" /> </RegisterGame>
//<?xml version="1.0" encoding="utf-8" ?> <GetGames xmlns="https://se2.mini.pw.edu.pl/17-results/" />
//<?xml version="1.0" encoding="utf-8" ?> <JoinGame xmlns="https://se2.mini.pw.edu.pl/17-results/" gameName="easyGame" preferedRole="leader" preferedTeam="blue" />
//<?xml version="1.0" encoding="utf-8" ?> <ConfirmJoiningGame xmlns="https://se2.mini.pw.edu.pl/17-results/" gameId="1" playerId="2" privateGuid="c094cab7-da7b-457f-89e5-a5c51756035f"> <PlayerDefinition id="2" team="blue" role="member"/> </ConfirmJoiningGame>
//<?xml version="1.0" encoding="utf-8"?> <Data xmlns="https://se2.mini.pw.edu.pl/17-results/" playerId="2" gameFinished="false"> <TaskFields> <TaskField x="1" y="5" timestamp="2017-02-23T17:20:11" distanceToPiece="5" /> <TaskField x="1" y="4" timestamp="2017-02-23T17:20:13" distanceToPiece="4" /> </TaskFields> <GoalFields> <GoalField x="0" y="9" timestamp="2017-02-23T17:20:17" team="blue" type="non-goal"/> <GoalField x="1" y="9" timestamp="2017-02-23T17:20:19" team="blue" type="goal" playerId="2"/> </GoalFields> <Pieces> <Piece id="1" timestamp="2017-02-23T17:20:09" type="sham" /> <Piece id="2" timestamp="2017-02-23T17:19:09" type="unknown" /> </Pieces> </Data>
//<?xml version="1.0" encoding="utf-8"?> <Game xmlns="https://se2.mini.pw.edu.pl/17-results/" playerId="2"> <Players> <Player team="red" role="leader" id="5" /> <Player team="red" role="member" id="6" /> <Player team="blue" role="leader" id="1" /> <Player team="blue" role="member" id="2" /> </Players> <Board width="5" tasksHeight="5" goalsHeight="3" /> <PlayerLocation x="0" y="3" /> </Game>