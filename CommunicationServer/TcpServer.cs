﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Xml;
using System.Xml.Serialization;
using XMLParsing;
using static CommunicationServer.Utils;

static class Constants
{
    public const bool OUTLOG = true;
    public const bool INLOG = true;
}

namespace CommunicationServer
{
    public class TcpServer
    {
        private static ulong _id;
        private static readonly object IdLocker = new object();

        private Dictionary<ulong, GameDTO> idToGame = new Dictionary<ulong, GameDTO>();
        static readonly object idToGameLocker = new object();

        List<JoinQueue> playersToRegister = new List<JoinQueue>();
        static readonly object playersToRegisterLocker = new object();

        List<NetworkStream> freeGMs = new List<NetworkStream>();
        static readonly object freeGMsLocker = new object();

        private TcpListener _server;
        private Boolean _isRunning;

        public void Run(int port)
        {
            _server = new TcpListener(IPAddress.Parse("0.0.0.0"), port);
            Console.WriteLine("Starting server on port:{0}", port);
            _server.Start();
            _isRunning = true;

            LoopClients();
        }

        private void LoopClients()
        {
            while (_isRunning)
            {
                TcpClient newClient = _server.AcceptTcpClient();
                LogVarious("New client connected.");
                Thread t = new Thread(HandleClient);
                t.Start(newClient);
            }
        }

        private void HandleClient(object obj)
        {
            TcpClient client = (TcpClient) obj;
            NetworkStream networkStream = client.GetStream();

            var buffer = new byte[4096];
            while (true)
            {
                int received;
                try
                {
                    received = networkStream.Read(buffer, 0, buffer.Length);
                    if (received <= 0)
                        continue;
                }
                catch (IOException)
                {
                    break;
                }

                string xmlMessage = Encoding.UTF8.GetString(buffer, 0, received);
                LogInput(xmlMessage);
                xmlMessage = xmlMessage.Trim('\uFEFF', '\u200B'); // Deleting BOMs for certainity.
                var singleMessages = Regex.Split(xmlMessage, @"(?=<\?xml version)");
                singleMessages = singleMessages.Where(x => !string.IsNullOrEmpty(x)).ToArray();
                if (singleMessages.Length > 1)
                {
                    LogAlert("There is more than one message");
                }

                foreach (var singleMessage in singleMessages)
                {
                    XmlDocument doc = new XmlDocument();
                    try
                    {
                        doc.LoadXml(singleMessage);
                    }
                    catch (Exception e)
                    {
                        LogAlert(string.Format("Cannot parse {0}", singleMessage));
                        LogAlert(e.ToString());
                        break;
                    }

                    MemoryStream messageStream = new MemoryStream(Encoding.UTF8.GetBytes(singleMessage));
                    string root = doc.DocumentElement.Name;
                    switch (root)
                    {
                        case "GetGames": //+
                            GetGamesCs(networkStream);
                            break;
                        case "RegisterGame": //+
                            RegisterGame(messageStream, networkStream);
                            break;
                        case "JoinGame": //+
                            JoinGame(messageStream, networkStream);
                            break;
                        case "Move":
                        case "Discover":
                        case "PickUpPiece":
                        case "PlacePiece":
                        case "DestroyPiece":
                        case "AuthorizeKnowledgeExchange":
                        case "TestPiece":
                            GameMessageToGm(messageStream, networkStream);
                            break;
                        case "ConfirmJoiningGame":
                            ConfirmJoiningGameMessage(messageStream, networkStream);
                            break;
                        case "RejectJoiningGame": //P
                            RejectJoiningGameMessage(messageStream, networkStream);
                            break;
                        case "Data":
                            DataMessage(messageStream, networkStream);
                            break;
                        case "KnowledgeExchangeRequest":
                            KnowledgeExchangeRequestMessage(messageStream, networkStream);
                            break;
                        case "RejectKnowledgeExchange":
                            RejectOrAcceptKnowledgeExchangeMessage(messageStream, networkStream);
                            break;
                        case "AcceptExchangeRequest":
                            RejectOrAcceptKnowledgeExchangeMessage(messageStream, networkStream);
                            break;
                        case "Game":
                            GameMessageDisposer(messageStream, networkStream);
                            break;

                        // TODO implemented to this point
                        case "GameStarted": //+
                            GameStarted(messageStream, networkStream);
                            break;
                        // TODO various messages not received but observed
                        // Player disconnected, Gm disconected
                    }

                    messageStream.Close();
                }
            }

            HandleDisconnection(networkStream);
        }

        private void GameMessageDisposer(MemoryStream messageStream, NetworkStream networkStream)
        {
            XmlSerializer ser = new XmlSerializer(typeof(Game));
            Game message = ser.Deserialize(messageStream) as Game;

            if (message == null)
            {
                throw new Exception("Error serializing RegisterGame.xml");
            }

            NetworkStream playerNetworkStream;
            lock (idToGameLocker)
            {
                var thisGame = idToGame.First(s => s.Value.GameMaster == networkStream).Value;
                playerNetworkStream = thisGame.Players.First(s => s.PlayerIdField == message.playerId).PlayerSocket;
            }

            byte[] bytes = messageStream.ToArray();
            try
            {
                playerNetworkStream?.Write(bytes, 0, bytes.Length);
            }
            catch (IOException)
            {
                HandleDisconnection(playerNetworkStream);
            }
        }

        private void RejectOrAcceptKnowledgeExchangeMessage(MemoryStream messageStream, NetworkStream networkStream)
        {
            XmlSerializer ser = new XmlSerializer(typeof(BetweenPlayersMessage));
            BetweenPlayersMessage message = ser.Deserialize(messageStream) as BetweenPlayersMessage;

            if (message == null)
            {
                throw new Exception("Error serializing RegisterGame.xml");
            }

            NetworkStream targetNetworkStream;
            lock (idToGameLocker)
            {
                bool isItFromGm = idToGame.Any(s => s.Value.GameMaster == networkStream);
                if (isItFromGm)
                {
                    LogVarious("RejectKnowledgeExchange message from gm sending to coresponding player");
                    var thisGame = idToGame.First(s => s.Value.GameMaster == networkStream).Value;
                    targetNetworkStream = thisGame.Players.First(s => s.PlayerIdField == message.playerId)
                        .PlayerSocket;
                }
                else
                {
                    LogVarious("RejectKnowledgeExchange message from player sending to coresponding player");
                    var thisGame =
                        idToGame.First(d => d.Value.Players.Any(s => s.PlayerIdField == message.playerId)).Value;
                    targetNetworkStream = thisGame.GameMaster;
                }
            }

            byte[] bytes = messageStream.ToArray();
            try
            {
                targetNetworkStream?.Write(bytes, 0, bytes.Length);
            }
            catch (IOException)
            {
                HandleDisconnection(targetNetworkStream);
            }
        }

        private void KnowledgeExchangeRequestMessage(MemoryStream messageStream, NetworkStream networkStream)
        {
            XmlSerializer ser = new XmlSerializer(typeof(KnowledgeExchangeRequest));
            KnowledgeExchangeRequest message = ser.Deserialize(messageStream) as KnowledgeExchangeRequest;

            if (message != null)
            {
                NetworkStream playerNetworkStream;
                lock (idToGameLocker)
                {
                    var thisGame = idToGame.First(s => s.Value.GameMaster == networkStream).Value;
                    playerNetworkStream = thisGame.Players.First(s => s.PlayerIdField == message.playerId).PlayerSocket;
                }

                byte[] bytes = messageStream.ToArray();
                try
                {
                    playerNetworkStream?.Write(bytes, 0, bytes.Length);
                }
                catch (IOException)
                {
                    HandleDisconnection(playerNetworkStream);
                }
            }
        }

        private void DataMessage(MemoryStream messageStream, NetworkStream networkStream)
        {
            XmlSerializer ser = new XmlSerializer(typeof(Data));
            Data message = ser.Deserialize(messageStream) as Data;

            if (message == null)
            {
                throw new Exception("Error serializing RegisterGame.xml");
            }

            NetworkStream targetNetworkStream;
            lock (idToGameLocker)
            {
                bool isItFromGm = idToGame.Any(s => s.Value.GameMaster == networkStream);
                if (isItFromGm)
                {
                    LogVarious("Data message from gm sending to coresponding player");
                    KeyValuePair<ulong, GameDTO> thisGameKeypair;
                    GameDTO thisGame;
                    try
                    {
                        thisGameKeypair = idToGame.First(s => s.Value.GameMaster == networkStream);
                        thisGame = thisGameKeypair.Value;
                        targetNetworkStream = thisGame.Players.First(s => s.PlayerIdField == message.playerId)
                            .PlayerSocket;
                    }
                    catch (InvalidOperationException e)
                    {
                        LogVarious(e.ToString());
                        LogVarious("Cannot find player to dispose message.");
                        return;
                    }

                    if (message.gameFinished) // If game is finished delete game and push this gm to free gm.
                    {
                        LogVarious("Game ended. Deleting this game and adding gm to freeGms");
                        lock (freeGMsLocker)
                        {
                            freeGMs.Add(thisGame.GameMaster);
                            idToGame.Remove(thisGameKeypair.Key);
                        }
                    }
                }
                else
                {
                    LogVarious("Data message from player sending to coresponding player");
                    var thisGame =
                        idToGame.First(d => d.Value.Players.Any(s => s.PlayerIdField == message.playerId)).Value;
                    targetNetworkStream = thisGame.GameMaster;
                }
            }

            byte[] bytes = messageStream.ToArray();
            try
            {
                targetNetworkStream?.Write(bytes, 0, bytes.Length);
            }
            catch (IOException)
            {
                HandleDisconnection(targetNetworkStream);
                return;
            }
            LogOutput(messageStream);
        }

        private void ConfirmJoiningGameMessage(MemoryStream messageStream, NetworkStream networkStream)
        {
            XmlSerializer ser = new XmlSerializer(typeof(ConfirmJoiningGame));
            ConfirmJoiningGame confirmJoiningGame = ser.Deserialize(messageStream) as ConfirmJoiningGame;

            if (confirmJoiningGame == null)
            {
                throw new Exception("Error serializing RegisterGame.xml");
            }

            NetworkStream playerStream;
            lock (idToGameLocker)
            {
                var thisGame = idToGame[confirmJoiningGame.gameId];
                string gameName = thisGame.GameInfoField.gameName;

                lock (playersToRegisterLocker)
                {
                    var playerIndex = playersToRegister.FindIndex(s => s.GameName == gameName);
                    playerStream = playersToRegister[playerIndex].PlayerStream;
                    playersToRegister.RemoveAt(playerIndex);
                }

                thisGame.Players.Add(new PlayerDTO(confirmJoiningGame.PlayerDefinition.id,
                    confirmJoiningGame.PlayerDefinition.role, confirmJoiningGame.PlayerDefinition.team,
                    playerStream));
            }

            byte[] bytes = messageStream.ToArray();
            try
            {
                playerStream?.Write(bytes, 0, bytes.Length);
            }
            catch (IOException)
            {
                HandleDisconnection(playerStream);
            }
        }

        private void RejectJoiningGameMessage(MemoryStream messageStream, NetworkStream networkStream)
        {
            XmlSerializer ser = new XmlSerializer(typeof(RejectJoiningGame));
            RejectJoiningGame message = ser.Deserialize(messageStream) as RejectJoiningGame;

            if (message == null)
            {
                throw new Exception("Error serializing RegisterGame.xml");
            }

            NetworkStream playerStream;
            lock (playersToRegisterLocker)
            {
                var playerIndex = playersToRegister.FindIndex(s => s.GameName == message.gameName);
                playerStream = playersToRegister[playerIndex].PlayerStream;
                playersToRegister.RemoveAt(playerIndex);
            }

            byte[] bytes = messageStream.ToArray();
            try
            {
                playerStream?.Write(bytes, 0, bytes.Length);
            }
            catch (IOException)
            {
                HandleDisconnection(playerStream);
            }
        }

        private void
            GetGamesCs(NetworkStream networkStream)
        {
            RegisteredGames registeredGames = new RegisteredGames {GameInfo = new GameInfo[] { }};
            List<GameInfo> gamesInfos = new List<GameInfo>();
            lock (idToGameLocker)
                foreach (var game in idToGame.Values)
                {
                    var currentBluePlayers = (ulong) game.Players.Count(s => s.PlayerTeamColour == TeamColour.blue);
                    var currentRedPlayers = (ulong) game.Players.Count(s => s.PlayerTeamColour == TeamColour.red);
                    var gameInfo = new GameInfo
                    {
                        gameName = game.GameInfoField.gameName,
                        blueTeamPlayers =
                            game.GameInfoField.blueTeamPlayers - currentBluePlayers,
                        redTeamPlayers = game.GameInfoField.redTeamPlayers - currentRedPlayers
                    };
                    gamesInfos.Add(gameInfo);
                }

            registeredGames.GameInfo = gamesInfos.ToArray();
            // Generate Response
            XmlSerializer serializer = new XmlSerializer(typeof(RegisteredGames));

            var memoryStream = new MemoryStream();
            using (StreamWriter streamWriter = new StreamWriter(memoryStream, new UTF8Encoding(false)))
            {
                serializer.Serialize(streamWriter, registeredGames);
                byte[] utf8EncodedXml = memoryStream.ToArray();
                networkStream.Write(utf8EncodedXml, 0, utf8EncodedXml.Length);
                LogOutput(memoryStream);
            }
        }

        private void RegisterGame(MemoryStream xmlMessage, NetworkStream networkStream)
        {
            XmlSerializer ser = new XmlSerializer(typeof(RegisterGame));
            RegisterGame registerGame = ser.Deserialize(xmlMessage) as RegisterGame;

            if (registerGame == null)
            {
                throw new Exception("Error serializing RegisterGame.xml");
            }

            ulong newId;
            lock (IdLocker)
            {
                newId = ++_id;
            }

            lock (idToGameLocker)
            {
                idToGame[newId] = new GameDTO(networkStream, registerGame.NewGameInfo, newId);
            }

            ConfirmGameRegistration cgr = new ConfirmGameRegistration {gameId = newId};

            // Generate Response
            XmlSerializer serializer = new XmlSerializer(typeof(ConfirmGameRegistration));
            var memoryStream = new MemoryStream();
            using (StreamWriter streamWriter = new StreamWriter(memoryStream, new UTF8Encoding(false)))
            {
                serializer.Serialize(streamWriter, cgr);
                byte[] utf8EncodedXml = memoryStream.ToArray();
                networkStream.Write(utf8EncodedXml, 0, utf8EncodedXml.Length);
                LogOutput(memoryStream);
            }
        }

        private void JoinGame(MemoryStream xmlMessage, NetworkStream networkStream)
        {
            XmlSerializer ser = new XmlSerializer(typeof(JoinGame));
            JoinGame joinGame = ser.Deserialize(xmlMessage) as JoinGame;

            if (joinGame == null)
            {
                throw new Exception("Error serializing JoinGame.xml");
            }

            NetworkStream masterStream;
            lock (idToGameLocker)
            {
                var thisGame = idToGame.First(s => s.Value.GameInfoField.gameName == joinGame.gameName);
                masterStream = thisGame.Value.GameMaster;
            }

            byte[] bytes = xmlMessage.ToArray();
//            bytes.Append((byte)23);
            try
            {
                masterStream?.Write(bytes, 0, bytes.Length);
                lock (playersToRegisterLocker)
                {
                    playersToRegister.Add(new JoinQueue(networkStream, joinGame.gameName));
                }
            }
            catch (IOException)
            {
                HandleDisconnection(networkStream);
                return;
            }

            LogOutput(xmlMessage);
        }

        private void GameMessageToGm(MemoryStream xmlMessage, NetworkStream networkStream)
        {
            NetworkStream gmNetworkStream;
            lock (idToGameLocker
            ) // TODO this way finds gm by finding this player gamesocket. It'd better to deserialize message to GameMessage but i couldn't make it work.
            {
                var thisGame =
                    idToGame.First(d => d.Value.Players.Any(s => s.PlayerSocket == networkStream)).Value;
                gmNetworkStream = thisGame.GameMaster;
            }

            byte[] bytes = xmlMessage.ToArray();
            try
            {
                gmNetworkStream.Write(bytes, 0, bytes.Length);
            }
            catch (IOException)
            {
                HandleDisconnection(networkStream);
                return;
            }

            LogOutput(xmlMessage);
        }

        private void GameStarted(MemoryStream xmlMessage, NetworkStream networkStream)
        {
            XmlSerializer ser = new XmlSerializer(typeof(GameStarted));
            GameStarted message = ser.Deserialize(xmlMessage) as GameStarted;

            if (message == null)
            {
                throw new Exception("Error serializing the xml");
            }

            XmlSerializer serializer = new XmlSerializer(typeof(Move));
            var memoryStream = new MemoryStream();
            using (StreamWriter streamWriter = new StreamWriter(memoryStream, new UTF8Encoding(false)))
            {
                serializer.Serialize(streamWriter, message);
                byte[] utf8EncodedXml = memoryStream.ToArray();
                networkStream.Write(utf8EncodedXml, 0, utf8EncodedXml.Length);
            }
        }

        private void HandleDisconnection(NetworkStream networkStream)
        {
            bool isItFromGm;
            lock (idToGameLocker)
            {
                isItFromGm = idToGame.Any(s => s.Value.GameMaster == networkStream);
            }

            if (isItFromGm)
            {
                LogAlert("Game master disconected"); // TODO send messages to players about that and delete game.
                KeyValuePair<ulong, GameDTO> thisGameKeypair;
                lock (idToGameLocker)
                {
                    thisGameKeypair = idToGame.First(d => d.Value.GameMaster == networkStream);
                }

                // Inform all players
                GameMasterDisconnected gameMasterDisconnected =
                    new GameMasterDisconnected {gameId = thisGameKeypair.Key};
                XmlSerializer serializer = new XmlSerializer(typeof(GameMasterDisconnected));
                var memoryStream = new MemoryStream();
                using (StreamWriter streamWriter = new StreamWriter(memoryStream, new UTF8Encoding(false)))
                {
                    serializer.Serialize(streamWriter, gameMasterDisconnected);
                    byte[] bytes = memoryStream.ToArray();
                    foreach (var player in thisGameKeypair.Value.Players)
                    {
                        try
                        {
                            player.PlayerSocket?.Write(bytes, 0, bytes.Length);
                        }
                        catch (IOException)
                        {
                            HandleDisconnection(player.PlayerSocket);
                            return;
                        }

                        LogOutput(memoryStream);
                    }
                }

                // Remove this game from dict
                lock (idToGameLocker)
                {
                    idToGame.Remove(thisGameKeypair.Key);
                }
            }
            else
            {
                LogAlert("Client disconected");
                NetworkStream gmStream;
                PlayerDTO thisPlayer;
                lock (idToGameLocker)
                {
                    try
                    {
                        var thisGame = idToGame.First(d => d.Value.Players.Any(s => s.PlayerSocket == networkStream))
                            .Value;
                        thisPlayer = thisGame.Players.First(x => x.PlayerSocket == networkStream);
                        thisGame.Players.RemoveAll(x => x.PlayerSocket == networkStream); // Remove player
                        gmStream = thisGame.GameMaster;
                    }
                    catch (Exception e)
                    {
                        LogVarious("Cannot delete player");
                        LogVarious(e.ToString());
                        return;
                    }
                }

                PlayerDisconnected playerDisconnected = new PlayerDisconnected {playerId = thisPlayer.PlayerIdField};
                XmlSerializer serializer = new XmlSerializer(typeof(PlayerDisconnected));
                var memoryStream = new MemoryStream();
                using (StreamWriter streamWriter = new StreamWriter(memoryStream, new UTF8Encoding(false)))
                {
                    serializer.Serialize(streamWriter, playerDisconnected);
                    byte[] bytes = memoryStream.ToArray();
                    try
                    {
                        gmStream?.Write(bytes, 0, bytes.Length);
                    }
                    catch (IOException)
                    {
                        HandleDisconnection(gmStream);
                        return;
                    }

                    LogOutput(memoryStream);
                }
            }
        }
    }
}