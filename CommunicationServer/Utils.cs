﻿using System;
using System.IO;
using System.Text;

namespace CommunicationServer
{
    public class Utils
    {
        private static string GetTime()
        {
            return DateTime.Now.ToString("h:mm:ss tt");
        }

        public static void LogOutput(MemoryStream memoryStream)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            string xmlMessage = Encoding.UTF8.GetString(memoryStream.ToArray(), 0, (int) memoryStream.Length);
            var communicate = string.Format("{0}:{1}", GetTime(), xmlMessage) + Environment.NewLine;
            Console.Write(communicate);
            Console.ResetColor();
            if (Constants.OUTLOG)
            {
                File.AppendAllText(@"output_log.txt", communicate);
            }
        }

        public static void LogInput(string xmlMessage)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            var communicate = string.Format("{0}:{1}", GetTime(), xmlMessage) + Environment.NewLine;
            Console.Write(communicate);
            Console.ResetColor();
            if (Constants.INLOG)
            {
                File.AppendAllText(@"input_log.txt", communicate);
            }
        }

        public static void LogVarious(string message)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("{0}:{1}", GetTime(), message);
            Console.ResetColor();
        }

        public static void LogAlert(string message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("{0}:{1}", GetTime(), message);
            Console.ResetColor();
        }
    }
}