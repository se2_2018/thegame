﻿using Xunit;
using System.Linq;
using System.Threading;

namespace GameMaster.Tests
{
    public class GameStateTests
    {
        [Fact]
        public void GeneratingPiecessOnDifferentPossitionsTest()
        {
            XmlParser pars = new XmlParser("Test.xml");
            pars.Parse();
            
            Game game = new Game(pars);

            bool result = true;
            if (game._gameState.pieces[0].Location == game._gameState.pieces[1].Location
                || game._gameState.pieces[0].Location == game._gameState.pieces[2].Location||
                game._gameState.pieces[2].Location == game._gameState.pieces[1].Location)
                result = false;

            Assert.True(result);
        }
        [Fact]
        public void AmountOfPiecesTest()
        {
            XmlParser pars = new XmlParser("Test.xml");
            pars.Parse();

            Game game = new Game(pars);
            int PiecesCount = game._gameState.pieces.Count;
            Assert.Equal(3,PiecesCount);
        }
        [Fact]
        public void PiecesInTaskAreaTest()
        {
            XmlParser pars = new XmlParser("Test.xml");
            pars.Parse();

            Game game = new Game(pars);
            bool result = true;
            foreach (var piece in game._gameState.pieces)
                if (piece.Location.y != 1)
                    result = false;
            Assert.True(result);
        }
        [Fact]
        public void GoalsInGoalAreaTest()
        {
            XmlParser pars = new XmlParser("Test.xml");
            pars.Parse();

            Game game = new Game(pars);
            bool result = true;
            foreach (var goal in game._gameState.redGoals)
                if (goal.Location.y != 2)
                    result = false;
            Assert.True(result);
        }
        [Fact]
        public void EmptyPlayerListTest()
        {
            XmlParser pars = new XmlParser("Test.xml");
            pars.Parse();

            Game game = new Game(pars);

            Assert.Empty(game._redPlayers);
        }
    }
}