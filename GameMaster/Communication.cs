﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using XMLParsing;


//TODO start game when all players join
// if more than one leader joins change to member
// if more red players change to blue
// check displaying of goals and pieces
// check generation of ids
namespace GameMaster
{
    public class Communication
    {
        private string _hostName;
        private int _portNumber;

        private TcpClient _tcpClient;
        private NetworkStream _networkStream;

        private Game _game;
        private List<KnowledgeExchangeRequest> requests;

        public Communication(string hostName, int portNumber, Game game)
        {
            _hostName = hostName;
            _portNumber = portNumber;
            _game = game;

            requests = new List<KnowledgeExchangeRequest>();
            Connect();
        }

        private void Connect()
        {
            try
            {
                _tcpClient = new TcpClient(_hostName, _portNumber);

                if (_tcpClient.Connected == false)
                {
                    Console.WriteLine(@"Connection refused!");
                    return;
                }

                Thread _tcpMessageReceiverThread = new Thread(ReceiveMessage);
                _tcpMessageReceiverThread.Start();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        private void ReceiveMessage()
        {
            _networkStream = _tcpClient.GetStream();
            byte[] bytes = new byte[4096];

            while (true)
            {
                if (_networkStream.DataAvailable)
                {
                    int bytesRead = _networkStream.Read(bytes, 0, bytes.Length);
                    string xmlMessage = Encoding.UTF8.GetString(bytes, 0, bytesRead);
                    Console.Write(@"New message received (" + xmlMessage + "): ");
                    ParseMessage(xmlMessage);
                }
            }
        }

        int id = 0;

        private void ParseMessage(string xmlMessage)
        {
            var singleMessages = Regex.Split(xmlMessage, @"(?=<\?xml version)");
            singleMessages = singleMessages.Where(x => !string.IsNullOrEmpty(x)).ToArray();
            foreach (var singleMessage in singleMessages)
            {
                XmlDocument doc = new XmlDocument();
                try
                {
                    doc.LoadXml(singleMessage);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Cannot parse {0}", singleMessage);
                    Console.WriteLine(e.ToString());
                    break;
                }

                var byteArray = Encoding.UTF8.GetBytes(singleMessage);
                var stream = new MemoryStream(byteArray);
                string messageType = doc.DocumentElement.Name;
                switch (messageType)
                {
                    case "ConfirmGameRegistration":
                        XmlSerializer ser = new XmlSerializer(typeof(ConfirmGameRegistration));
                        ConfirmGameRegistration registerGame = ser.Deserialize(stream) as ConfirmGameRegistration;
                        Console.WriteLine("Game registered with id:");
                        Console.WriteLine(registerGame.gameId);

                        _game.Id = (int) registerGame.gameId;
                        break;

                    case "JoinGame":
                        ser = new XmlSerializer(typeof(JoinGame));
                        JoinGame joinGame = ser.Deserialize(stream) as JoinGame;
                        XMLParsing.Player player = new XMLParsing.Player();
                        Guid guid = Guid.NewGuid();

                        TeamColour colour = joinGame.preferredTeam;

                        if (colour == TeamColour.blue)
                        {
                            if (_game._bluePlayers.Count == _game._bluePlayersCount)
                                colour = TeamColour.red;
                        }

                        if (colour == TeamColour.red)
                        {
                            if (_game._redPlayers.Count == _game._redPlayersCount)
                                colour = TeamColour.blue;
                        }

                        PlayerRole playerRole = joinGame.preferredRole;

                        if (playerRole == PlayerRole.leader && colour == TeamColour.red && _game._redHaveLeader)
                            playerRole = PlayerRole.member;
                        if (playerRole == PlayerRole.leader && colour == TeamColour.red && !_game._redHaveLeader)
                            _game._redHaveLeader = true;


                        if (playerRole == PlayerRole.leader && colour == TeamColour.blue && _game._blueHaveLeader)
                            playerRole = PlayerRole.member;
                        if (playerRole == PlayerRole.leader && colour == TeamColour.blue && !_game._blueHaveLeader)
                            _game._blueHaveLeader = true;

                        switch (colour)
                        {
                            case TeamColour.blue:
                            {
                                player.id = Convert.ToUInt64(id);
                                player.team = colour;
                                player.role = playerRole;

                                Player gmPlayer = new Player();
                                gmPlayer.id = Convert.ToUInt64(id);
                                gmPlayer.team = colour;
                                gmPlayer.role = playerRole;
                                gmPlayer.guid = guid.ToString();
                                _game._bluePlayers.Add(gmPlayer);

                                id++;
                                break;
                            }
                            case TeamColour.red:
                            {
                                player.id = Convert.ToUInt64(id);
                                player.team = colour;
                                player.role = playerRole;

                                Player gmPlayer = new Player();
                                gmPlayer.id = Convert.ToUInt64(id);
                                gmPlayer.team = colour;
                                gmPlayer.role = playerRole;
                                gmPlayer.guid = guid.ToString();
                                _game._redPlayers.Add(gmPlayer);

                                id++;
                                break;
                            }
                        }

                        ConfirmJoiningGame confirm = new ConfirmJoiningGame();
                        confirm.PlayerDefinition = player;
                        confirm.gameId = Convert.ToUInt64(_game.Id);
                        confirm.privateGuid = guid.ToString();

                        SendMessage(confirm);

                        if (CheckGame()) StartGame();
                        break;

                    case "Discover":
                        XmlSerializer serDiscover = new XmlSerializer(typeof(Discover));
                        Discover Discover = serDiscover.Deserialize(stream) as Discover;

                        Player playerDisc = _game._redPlayers.Find(p => p.guid == Discover.playerGuid.ToString());
                        if (playerDisc == null)
                            playerDisc = _game._bluePlayers.Find(p => p.guid == Discover.playerGuid.ToString());

                        Dictionary<Location, int> taskDistances = DiscoverHandle(playerDisc);

                        Data dataDisc = new Data();
                        dataDisc.playerId = playerDisc.id;
                        dataDisc.gameFinished = _game.won;

                        List<TaskField> taskFields = new List<TaskField>();

                        foreach (KeyValuePair<Location, int> entry in taskDistances)
                        {
                            TaskField taskField = new TaskField();
                            taskField.x = entry.Key.x;
                            taskField.y = entry.Key.y;
                            taskField.distanceToPiece = entry.Value;
                            taskFields.Add(taskField);
                        }

                        dataDisc.TaskFields = taskFields.ToArray();
                        SendMessage(dataDisc);
                        break;

                    case "Move":
                        XmlSerializer serMove = new XmlSerializer(typeof(Move));
                        Move moveMessage = serMove.Deserialize(stream) as Move;

                        Player playerMove = _game._redPlayers.Find(p => p.guid == moveMessage.playerGuid.ToString());
                        if (playerMove == null)
                            playerMove = _game._bluePlayers.Find(p => p.guid == moveMessage.playerGuid.ToString());

                        Data dataMove = new Data();
                        dataMove.playerId = playerMove.id;
                        dataMove.gameFinished = _game.won;

                        Location loc = new Location();
                        loc.x = playerMove.Location.x;
                        loc.y = playerMove.Location.y;

                        switch (moveMessage.direction)
                        {
                            case MoveType.up:
                            {
                                Location tmp = new Location();
                                tmp.x = playerMove.Location.x;
                                tmp.y = playerMove.Location.y - 1;
                                if (playerMove.Location.y - 1 >= 0 && IsFree(tmp))
                                {
                                    loc = tmp;
                                }

                                break;
                            }
                            case MoveType.down:
                            {
                                Location tmp = new Location();
                                tmp.x = playerMove.Location.x;
                                tmp.y = playerMove.Location.y + 1;
                                if (playerMove.Location.y + 1 < _game._rowCount && IsFree(tmp))
                                {
                                    loc = tmp;
                                }

                                break;
                            }
                            case MoveType.left:
                            {
                                Location tmp = new Location();
                                tmp.x = playerMove.Location.x - 1;
                                tmp.y = playerMove.Location.y;
                                if (playerMove.Location.x + 1 >= 0 && IsFree(tmp))
                                {
                                    loc = tmp;
                                }

                                break;
                            }
                            case MoveType.right:
                            {
                                Location tmp = new Location();
                                tmp.x = playerMove.Location.x + 1;
                                tmp.y = playerMove.Location.y;
                                if (playerMove.Location.x < _game._columnCount && IsFree(tmp))
                                {
                                    loc = tmp;
                                }

                                break;
                            }
                        }

                        playerMove.Location = loc;
                        Thread.Sleep(_game.MoveDelay * 10);
                        dataMove.PlayerLocation = loc;
                        SendMessage(dataMove);
                        break;

                    case "PickUpPiece":
                        XmlSerializer serPickUp = new XmlSerializer(typeof(PickUpPiece));
                        PickUpPiece PickUp = serPickUp.Deserialize(stream) as PickUpPiece;

                        Player playerPickUp = _game._redPlayers.Find(p => p.guid == PickUp.playerGuid.ToString());
                        if (playerPickUp == null)
                            playerPickUp = _game._bluePlayers.Find(p => p.guid == PickUp.playerGuid.ToString());

                        Piece piecePick = PickUpHandle(playerPickUp);

                        Data dataPick = new Data();
                        dataPick.playerId = playerPickUp.id;
                        dataPick.gameFinished = _game.won;
                        if (piecePick != null)
                        {
                            var p = new XMLParsing.Piece
                            {
                                id = Convert.ToUInt64(piecePick.Id),
                                timestamp = DateTime.Now,
                                playerId = Convert.ToUInt64(playerPickUp.id),
                                type = PieceType.unknown
                            };
                            List<XMLParsing.Piece> l = new List<XMLParsing.Piece>();
                            l.Add(p);
                            dataPick.Pieces = l.ToArray();
                        }

                        SendMessage(dataPick);

                        break;

                    case "TestPiece":
                        XmlSerializer serTestPiece = new XmlSerializer(typeof(TestPiece));
                        TestPiece Test = serTestPiece.Deserialize(stream) as TestPiece;

                        Player playerTest = _game._redPlayers.Find(p => p.guid == Test.playerGuid.ToString());
                        if (playerTest == null)
                            playerTest = _game._bluePlayers.Find(p => p.guid == Test.playerGuid.ToString());

                        Piece pieceTest = TestPieceHandle(playerTest);

                        Data dataTest = new Data();
                        dataTest.playerId = playerTest.id;
                        dataTest.gameFinished = _game.won;
                        if (pieceTest != null)
                        {
                            var p = new XMLParsing.Piece();
                            p.id = Convert.ToUInt64(pieceTest.Id);
                            if (pieceTest.IsSham) p.type = PieceType.sham;
                            else p.type = PieceType.normal;
                            p.playerId = Convert.ToUInt64(playerTest.id);
                            p.timestamp = DateTime.Now;
                            List<XMLParsing.Piece> l = new List<XMLParsing.Piece>();
                            l.Add(p);
                            dataTest.Pieces = l.ToArray();
                        }

                        SendMessage(dataTest);
                        break;

                    case "PlacePiece":
                        XmlSerializer serPlacePiece = new XmlSerializer(typeof(PlacePiece));
                        PlacePiece Place = serPlacePiece.Deserialize(stream) as PlacePiece;

                        Player playerPlace = _game._redPlayers.Find(p => p.guid == Place.playerGuid.ToString());
                        if (playerPlace == null)
                            playerPlace = _game._bluePlayers.Find(p => p.guid == Place.playerGuid.ToString());

                        Goal goal = PlaceHandle(playerPlace);

                        Data dataPlace = new Data();
                        dataPlace.playerId = playerPlace.id;
                        dataPlace.gameFinished = _game.won;
                        if (goal != null)
                        {
                            var g = new XMLParsing.GoalField();
                            g.x = Convert.ToUInt32(goal.Location.x);
                            g.y = Convert.ToUInt32(goal.Location.y);
                            g.team = playerPlace.team;
                            g.playerId = Convert.ToUInt64(playerPlace.id);
                            g.timestamp = DateTime.Now;
                            g.type = GoalFieldType.goal;
                            List<GoalField> l = new List<GoalField>();
                            l.Add(g);
                            dataPlace.GoalFields = l.ToArray();
                        }

                        SendMessage(dataPlace);
                        break;

                    case "AuthorizeKnowledgeExchange":
                        XmlSerializer serAuthKnowEx = new XmlSerializer(typeof(AuthorizeKnowledgeExchange));
                        byteArray = Encoding.UTF8.GetBytes(xmlMessage);
                        stream = new MemoryStream(byteArray);
                        AuthorizeKnowledgeExchange AuthKnowEx =
                            serAuthKnowEx.Deserialize(stream) as AuthorizeKnowledgeExchange;

                        Player sender = _game._redPlayers.Find(p => p.guid == AuthKnowEx.playerGuid.ToString());
                        if (sender == null)
                            sender = _game._bluePlayers.Find(p => p.guid == AuthKnowEx.playerGuid.ToString());

                        Player receiver;
                        if (sender.team == TeamColour.red)
                        {
                            receiver = _game._redPlayers.Find(p => p.id == AuthKnowEx.withPlayerId);
                        }
                        else
                        {
                            receiver = _game._bluePlayers.Find(p => p.id == AuthKnowEx.withPlayerId);
                        }

                        if (receiver == null)
                        {
                            RejectKnowledgeExchange reject = new RejectKnowledgeExchange();
                            reject.permanent = true;
                            reject.playerId = sender.id;
                            reject.senderPlayerId = AuthKnowEx.withPlayerId;

                            SendMessage(reject);
                        }
                        else
                        {
                            KnowledgeExchangeRequest req = requests.Find(r =>
                                r.senderPlayerId == receiver.id && r.playerId == sender.id);
                            if (req == null)
                            {
                                Thread.Sleep(_game.KnowledgeExchangeDelay * 10);

                                KnowledgeExchangeRequest request = new KnowledgeExchangeRequest();
                                request.playerId = receiver.id;
                                request.senderPlayerId = sender.id;
                                requests.Add(request);

                                SendMessage(request);
                            }
                            else
                            {
                                Thread.Sleep(_game.KnowledgeExchangeDelay * 10);

                                AcceptExchangeRequest accept = new AcceptExchangeRequest();
                                accept.playerId = receiver.id;
                                accept.senderPlayerId = sender.id;
                                requests.Remove(req);

                                SendMessage(accept);
                            }
                        }

                        break;
                }
            }
        }

        private Dictionary<Location, int> DiscoverHandle(Player player)
        {
            var dict = new Dictionary<Location, int>();

            for (int i = Convert.ToInt16(player.Location.x) - 1; i <= Convert.ToInt16(player.Location.x) + 1; i++)
            {
                for (int j = Convert.ToInt16(player.Location.y) - 1; j <= Convert.ToInt16(player.Location.y) + 1; j++)
                {
                    if (i < 0 || i >= _game._columnCount || j < 0 || j >= _game._rowCount) continue;
                    Location tmp = new Location();
                    tmp.x = Convert.ToUInt16(i);
                    tmp.y = Convert.ToUInt16(j);
                    dict.Add(tmp, manhattanDistance(tmp));
                }
            }

            Thread.Sleep(_game.DiscoverDelay * 10);
            return dict;
        }

        private int manhattanDistance(Location loc)
        {
            int dist = _game._columnCount + _game._rowCount;

            foreach (var piece in _game._gameState.pieces)
            {
                int tmp = Math.Abs(Convert.ToInt16(loc.x) - Convert.ToInt16(piece.Location.x) +
                                   Math.Abs(Convert.ToInt16(loc.y) - Convert.ToInt16(piece.Location.y)));
                if (tmp < dist) dist = tmp;
            }

            return dist;
        }

        private bool IsFree(Location _location)
        {
            bool isFree = true;
            foreach (var bp in _game._bluePlayers)
            {
                if (bp.Location == _location)
                    isFree = false;
            }

            foreach (var rp in _game._bluePlayers)
            {
                if (rp.Location == _location)
                    isFree = false;
            }

            return isFree;
        }

        private Piece PickUpHandle(Player player)
        {
            foreach (Piece piece in _game._gameState.pieces)
            {
                if (player.Location != piece.Location) continue;
                if (piece.player != null) continue;
                piece.player = player;
                player.HasPiece = true;
                //game._gameState.pieces.Remove(piece); - do not removing from list, but checking if piece has its "player"
                Thread.Sleep(_game.PickUpDelay * 10);
                return piece;
            }

            return null;
        }

        //TODO player do not have an piece (written in issues)
        public Piece TestPieceHandle(Player player)
        {
            Piece piece = _game._gameState.pieces.Find(p => p.player == player);

            if (piece == null) throw new Exception("No piece to test");
            Thread.Sleep(_game.TestDelay * 10);
            return piece;
        }

        public Goal PlaceHandle(Player player)
        {
            Piece piece = _game._gameState.pieces.Find(p => p.player == player);

            if (piece == null) throw new Exception("No piece to place");
            if (player.team == TeamColour.red)
            {
                foreach (Goal goal in _game._gameState.redGoals)
                {
                    if (goal.Location != player.Location || goal.IsAchieved) continue;

                    _game.RedAchieved++;
                    goal.IsAchieved = true;
                    //removing from list of pieces piece that is on the goal
                    _game._gameState.pieces.Remove(piece);
                    Thread.Sleep(_game.PlacingDelay * 10);
                    if (_game.RedAchieved != _game._gameState.redGoals.Count) return goal;
                    Console.WriteLine("\n\t Red Team Won\n");
                    _game.won = true;

                    return goal;
                }
            }
            else
            {
                foreach (Goal goal in _game._gameState.blueGoals)
                {
                    if (goal.Location != player.Location || goal.IsAchieved) continue;
                    _game.BlueAchieved++;
                    goal.IsAchieved = true;
                    //removing from list of pieces piece that is on the goal
                    _game._gameState.pieces.Remove(piece);
                    Thread.Sleep(_game.PlacingDelay * 10);
                    if (_game.BlueAchieved != _game._gameState.blueGoals.Count) return goal;
                    Console.WriteLine("\n\t Blue Team Won\n");
                    _game.won = true;

                    return goal;
                }
            }

            return null;
        }

        public void SendMessage(object message)
        {
            _networkStream = _tcpClient.GetStream();
            XmlSerializer serializer = new XmlSerializer(message.GetType());

            var memoryStream = new MemoryStream();

            using (StreamWriter streamWriter = new StreamWriter(memoryStream, new UTF8Encoding(false)))
            {
                serializer.Serialize(streamWriter, message);
                byte[] utf8EncodedXml = memoryStream.ToArray();
                _networkStream.Write(utf8EncodedXml, 0, utf8EncodedXml.Length);
            }
        }

        public bool CheckGame()
        {
            bool canStart = _game._redPlayers.Count == _game._redPlayersCount &&
                            _game._bluePlayers.Count == _game._bluePlayersCount;
            return canStart;
        }

        public void StartGame()
        {
            _game._gameState.PlacePlayers();
            List<XMLParsing.Player> allPlayers = new List<XMLParsing.Player>();
            foreach (Player p in _game._redPlayers)
            {
                XMLParsing.Player _p = new XMLParsing.Player();
                _p.id = p.id;
                _p.role = p.role;
                _p.team = p.team;
                allPlayers.Add(_p);
            }

            foreach (Player p in _game._bluePlayers)
            {
                XMLParsing.Player _p = new XMLParsing.Player();
                _p.id = p.id;
                _p.role = p.role;
                _p.team = p.team;
                allPlayers.Add(_p);
            }

            foreach (XMLParsing.Player player in allPlayers)
            {
                Location loc;
                Player pp = _game._bluePlayers.Find(p => p.id == player.id);
                if (pp == null) pp = _game._redPlayers.Find(p => p.id == player.id);
                loc = pp.Location;

                XMLParsing.Game gameMessage = new XMLParsing.Game
                {
                    playerId = player.id,
                    PlayerLocation = loc,
                    Players = allPlayers.ToArray()
                };
                GameBoard gameBoard = new GameBoard
                {
                    width = (uint) _game._columnCount,
                    tasksHeight = (uint) _game._rowCountTaskArea,
                    goalsHeight = (uint) _game._rowCountGoalArea
                };
                gameMessage.Board = gameBoard;

                SendMessage(gameMessage);
            }
        }
    }
}