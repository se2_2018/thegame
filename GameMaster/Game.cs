﻿using System;
using System.Collections.Generic;
using System.Text;
using Client;
using System.Linq;

namespace GameMaster
{
    //TODO waiting for clients to create players

    public class Game
    {
        public int _rowCount => 2 * _rowCountGoalArea + _rowCountTaskArea;
        public int _columnCount { get; set; }
        public string _gameName { get; set; }
        public int _redPlayersCount { get; set; }
        public int _bluePlayersCount { get; set; }
        public float _probShamPiece { get; set; }
        public int _initialNumberOfPieces { get; set; }
        public int _rowCountTaskArea { get; set; }
        public int _rowCountGoalArea { get; set; }
        public int Id { get; set; }

        public int MoveDelay { get; set; }
        public int DiscoverDelay { get; set; }
        public int TestDelay { get; set; }
        public int PickUpDelay { get; set; }
        public int PlacingDelay { get; set; }
        public int KnowledgeExchangeDelay { get; set; }

        public GameState _gameState { get; set; }
        public List<Player> _redPlayers = new List<Player>();
        public List<Player> _bluePlayers = new List<Player>();

        public bool _redHaveLeader = false;
        public bool _blueHaveLeader = false;

        public XmlParser parser;

        public int RedAchieved = 0;
        public int BlueAchieved = 0;
        public bool won = false;

        public Game(XmlParser pars)
        {
            parser = pars;
            _gameName = pars.GameName;
            _redPlayersCount = pars.NumberOfPlayersPerTeam;
            _bluePlayersCount = pars.NumberOfPlayersPerTeam;
            _columnCount = pars.BoardWidth;
            _probShamPiece = pars.ShamProbability;
            _initialNumberOfPieces = pars.InitialNumberOfPieces;
            _rowCountTaskArea = pars.TaskAreaLength;
            _rowCountGoalArea = pars.GoalAreaLength;

            MoveDelay = pars.MoveDelay;
            DiscoverDelay = pars.DiscoverDelay;
            TestDelay = pars.TestDelay;
            PickUpDelay = pars.PickUpDelay;
            PlacingDelay = pars.PlacingDelay;
            KnowledgeExchangeDelay = pars.KnowledgeExchangeDelay;

            _gameState = new GameState(this);
        }

        public void GameInfo()
        {
            Console.WriteLine(@"Row count: " + _rowCount);
            Console.WriteLine(@"Column count: " + _columnCount);
            Console.WriteLine(@"Game name: " + _gameName);
            Console.WriteLine(@"Red team player: " + _redPlayersCount);
            Console.WriteLine(@"Blue team player: " + _bluePlayersCount);
            Console.WriteLine(@"Task area: " + _rowCountTaskArea);
            Console.WriteLine(@"Goal area: " + _rowCountGoalArea);
        }

        public void DisplayGameState()
        {
            for (int i = 0; i < _rowCount; i++)
            {
                for (int j = 0; j < _columnCount; j++)
                {
                    if (i + 1 > _rowCountGoalArea && i < _rowCountGoalArea + _rowCountTaskArea)
                    {
                        Console.BackgroundColor = ConsoleColor.Magenta;
                    }
                    else
                    {
                        Console.BackgroundColor = ConsoleColor.Black;
                    }

                    Location loc = new Location(j, i);
                    if (_gameState.redGoals.Any(p => p.Location.x == loc.x && p.Location.y == loc.y))
                    {
                        if (_gameState.redGoals.Any(p =>
                            p.Location.x == loc.x && p.Location.y == loc.y && p.IsAchieved))
                        {
                            Console.ForegroundColor = ConsoleColor.DarkYellow;
                        }
                        else
                        {
                            Console.ForegroundColor = ConsoleColor.Yellow;
                        }

                        Console.Write("G");
                        if (_redPlayers.Any(p => p.Location.x == loc.x && p.Location.y == loc.y))
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.Write("R ");
                        }
                        else
                        {
                            Console.Write("  ");
                        }
                    }
                    else if (_gameState.blueGoals.Any(p => p.Location.x == loc.x && p.Location.y == loc.y))
                    {
                        if (_gameState.blueGoals.Any(
                            p => p.Location.x == loc.x && p.Location.y == loc.y && p.IsAchieved))
                        {
                            Console.ForegroundColor = ConsoleColor.DarkYellow;
                        }
                        else
                        {
                            Console.ForegroundColor = ConsoleColor.Yellow;
                        }

                        Console.Write("G");
                        if (_bluePlayers.Any(p => p.Location.x == loc.x && p.Location.y == loc.y))
                        {
                            Console.ForegroundColor = ConsoleColor.Blue;
                            Console.Write("B ");
                        }
                        else
                        {
                            Console.Write("  ");
                        }
                    }
                    else if (_gameState.pieces.Any(p => p.Location.x == loc.x && p.Location.y == loc.y))
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.Write("P");
                        if (_bluePlayers.Any(p => p.Location.x == loc.x && p.Location.y == loc.y))
                        {
                            Console.ForegroundColor = ConsoleColor.Blue;
                            Console.Write("B ");
                        }
                        else
                        {
                            if (_redPlayers.Any(p => p.Location.x == loc.x && p.Location.y == loc.y))
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.Write("R ");
                            }
                            else
                            {
                                Console.Write("  ");
                            }
                        }
                    }
                    else if (_redPlayers.Exists(p => p.Location.x == loc.x && p.Location.y == loc.y))
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write("R  ");
                    }
                    else if (_bluePlayers.Exists(p => p.Location.x == loc.x && p.Location.y == loc.y))
                    {
                        Console.ForegroundColor = ConsoleColor.Blue;
                        Console.Write("B  ");
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.Write("X  ");
                    }
                }

                Console.WriteLine();
            }

            Console.ResetColor();
        }
    }
}
