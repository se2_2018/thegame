﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using XMLParsing;

namespace GameMaster
{
    public class Player : XMLParsing.Player
    {
        public Location Location;
        public bool Won;
        public string guid;
        public bool HasPiece;
        public Player()
        {

        }

    }
    public class Piece
    {
        public Piece(Location _location, int _Id, bool _IsSham)
        {
            Location = _location;
            Id = _Id;
            IsSham = _IsSham;
            player = null;
        }

        public int Id;
        public Location Location;
        public bool IsSham;
        public Player player;
    }

    public class Goal
    {
        public Goal(Location _Location, bool _IsAchieved)
        {
            Location = _Location;
            IsAchieved = _IsAchieved;
        }

        public Location Location;
        public bool IsAchieved;
    }

    public class GameState
    {
        public Game _game;

        public List<Piece> pieces = new List<Piece>();
        public List<Goal> redGoals = new List<Goal>();
        public List<Goal> blueGoals = new List<Goal>();
        Semaphore pieceSemaphore = new Semaphore(1, 1);
        public Thread GeneratingPiecesThread;

        public GameState(Game game)
        {
            _game = game;
            PlacePieces();
            PlaceGoals();
            //PlacePlayers();
            GeneratingPiecesThread = new Thread(ThreadFunction);
        }

        private void ThreadFunction()
        {
            while (true)
            {
                PlacePieces();
                Thread.Sleep(5000);
            }
        }

        private void PlacePieces()
        {
            Random rnd = new Random();
            var board = _GetBoardAsArrayStrings();
            int numberOfPieces = _game._initialNumberOfPieces;

            //randomizes the amount of shams, but makes sure that there is at least one non-sham piece
            int shams = rnd.Next(0, numberOfPieces - 1);

            for (int i = 0; i < numberOfPieces; i++)
            {
                //Board is splitted into 3 parts - one part for each team and one common part
                //Place pieces at the middle of this board
                int x, y;
                do
                {
                    x = rnd.Next(_game._columnCount);
                    y = rnd.Next(_game._rowCountGoalArea, _game._rowCountGoalArea + _game._rowCountTaskArea);
                } while (board[y, x] != "X");

                board[y, x] = "T";
                Location loc = new Location();
                loc.x = Convert.ToUInt16(x);
                loc.y = Convert.ToUInt16(y);
                bool isSham = i < shams;
                pieceSemaphore.WaitOne();
                pieces.Add(new Piece(loc, i, isSham));
                pieceSemaphore.Release();
            }
        }

        private void PlaceGoals()
        {
            blueGoals = _game.parser.blueGoals;
            redGoals = _game.parser.redGoals;
            var board = _GetBoardAsArrayStrings();

            foreach (Goal g in blueGoals)
            {
                int x = Convert.ToInt16(g.Location.x), y = Convert.ToInt16(g.Location.y);

                board[y, x] = "G";
                Location loc = new Location();
                loc.x = Convert.ToUInt16(x);
                loc.y = Convert.ToUInt16(y);
            }

            foreach (Goal g in redGoals)
            {
                uint x = g.Location.x, y = g.Location.y;

                board[y, x] = "G";
                Location loc = new Location();
                loc.x = Convert.ToUInt16(x);
                loc.y = Convert.ToUInt16(y);
            }
        }

        public void PlacePlayers()
        {
            Random rnd = new Random();
            var board = _GetBoardAsArrayStrings();

            foreach (Player player in _game._redPlayers)
            {
                int x, y;
                do
                {
                    x = rnd.Next(_game._columnCount);
                    y = rnd.Next(_game._rowCountGoalArea);
                } while (board[y, x] != "X" || board[y, x] == "RP"); //Prevents players from spawning in the same spot*

                board[y, x] = "RP";
                Location loc = new Location();
                loc.x = Convert.ToUInt16(x);
                loc.y = Convert.ToUInt16(y);
                player.Location = loc;
            }

            foreach (Player player in _game._bluePlayers)
            {
                int x, y;
                do
                {
                    x = rnd.Next(_game._columnCount);
                    y = rnd.Next(_game._rowCountGoalArea + _game._rowCountTaskArea, _game._rowCount);
                } while (board[y, x] != "X" || board[y, x] == "BP"); //same as'*'

                board[y, x] = "BP";
                Location loc = new Location();
                loc.x = Convert.ToUInt16(x);
                loc.y = Convert.ToUInt16(y);
                player.Location = loc;
            }
        }

        private string[,] _GetBoardAsArrayStrings()
        {
            var board = new string[_game._rowCount, _game._columnCount];
            for (int i = 0; i < _game._columnCount; i++)
            {
                for (int j = 0; j < _game._rowCount; j++)
                {
                    Location loc = new Location();
                    loc.x = Convert.ToUInt16(i);
                    loc.y = Convert.ToUInt16(j);
                    if (redGoals.Any(p => p.Location != null && p.Location.x == loc.x && p.Location.y == loc.y))
                    {
                        board[j, i] = "RG";
                    }
                    else if (blueGoals.Any(p => p.Location != null && p.Location.x == loc.x && p.Location.y == loc.y))
                    {
                        board[j, i] = "BG";
                    }
                    else if (pieces.Any(p => p.Location != null && p.Location.x == loc.x && p.Location.y == loc.y))
                    {
                        board[j, i] = "T";
                    }
                    else if (_game._redPlayers.Exists(p =>
                        p.Location != null && p.Location.x == loc.x && p.Location.y == loc.y))
                    {
                        board[j, i] = "RP";
                    }
                    else if (_game._bluePlayers.Exists(p =>
                        p.Location != null && p.Location.x == loc.x && p.Location.y == loc.y))
                    {
                        board[j, i] = "BP";
                    }
                    else
                    {
                        board[j, i] = "X";
                    }
                }
            }

            return board;
        }
    }
 }
