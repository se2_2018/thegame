﻿using System;
using System.IO;
using System.Xml.Serialization;
using XMLParsing;

namespace GameMaster
{
    class Program
    {

        static void Main(string[] args)
        {
            // TODO @Filip cleanup, and decide what to do with that.
            //            //NAJPIERW DODAJ PLIK Z BITBUCKETA (TEGO Z DOKUEMNTACJA) DO FOLDERU thegame/GameMaster/bin/Debug/netcoreapp2.0
            XmlParser pars = new XmlParser("Championship.xml");
            pars.Parse();

            Game game = new Game(pars);

            game.GameInfo();
            Console.Read();

            game.DisplayGameState();

            Communication communication = new Communication("127.0.0.1", 8008, game);

            RegisterGame registerGame = new RegisterGame();
            GameInfo gameInfo = new GameInfo();
            gameInfo.gameName = game._gameName;
            gameInfo.redTeamPlayers = Convert.ToUInt64(game._redPlayersCount);
            gameInfo.blueTeamPlayers = Convert.ToUInt64(game._bluePlayersCount);

            registerGame.NewGameInfo = gameInfo;
            communication.SendMessage(registerGame);
        }
    }
}