﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;
using Client;

namespace GameMaster
{
    public class XmlParser
    {
        public XmlDocument doc;
        public List<Goal> blueGoals = new List<Goal>();
        public List<Goal> redGoals = new List<Goal>();
        public float ShamProbability = 0.1f;
        public int PlacingNewPiecesFrequency = 1000;
        public int InitialNumberOfPieces = 4;
        public int BoardWidth = 5;
        public int TaskAreaLength = 7;
        public int GoalAreaLength = 3;
        public int NumberOfPlayersPerTeam = 4;
        public string GameName = "Our Game";
        public int MoveDelay = 100;
        public int DiscoverDelay = 450;
        public int TestDelay = 500;
        public int PickUpDelay = 100;
        public int PlacingDelay = 100;
        public int KnowledgeExchangeDelay = 1200;
        public int RetryRegisterGameInterval = 5000;
        public int RetryJoinGameInterval = 5000;
        public int KeepAliveInterval = 30000;

        public XmlParser(string file)
        {
            doc = new XmlDocument();
            doc.Load(file);
        }

        public void Parse()
        {
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
            string address = "https://se2.mini.pw.edu.pl/17-pl-19/17-pl-19/";
            
            foreach(XmlAttribute attribute in doc.DocumentElement.Attributes)
            {
                if (attribute.Name == "xmlns")
                    address = attribute.InnerText;
                if (attribute.Name == "RetryRegisterGameInterval")
                    RetryRegisterGameInterval = Int32.Parse(attribute.InnerText);
                if(attribute.Name == "RetryJoinGameInterval")
                    RetryJoinGameInterval = Int32.Parse(attribute.InnerText);
                if (attribute.Name == "KeepAliveInterval")
                    KeepAliveInterval = Int32.Parse(attribute.InnerText);

            }
            nsmgr.AddNamespace("mini", address);
            foreach (XmlNode node in doc.DocumentElement.SelectNodes("//mini:GameDefinition/mini:Goals", nsmgr))
            {
                int i = 0;
                string text = node.InnerText;
                if(node.Attributes[0].Value == "red")
                {
                    XMLParsing.Location location = new XMLParsing.Location();
                    location.x = UInt32.Parse(node.Attributes[1].Value);
                    location.y = UInt32.Parse(node.Attributes[2].Value);
                    redGoals.Add(new Goal(location,false));
                }
                if (node.Attributes[0].Value == "blue")
                {
                    XMLParsing.Location location = new XMLParsing.Location();
                    location.x = UInt32.Parse(node.Attributes[1].Value);
                    location.y = UInt32.Parse(node.Attributes[2].Value);
                    blueGoals.Add(new Goal(location, false));
                }
                i++;
            }
            if(doc.DocumentElement.SelectSingleNode("//mini:GameDefinition/mini:ShamProbability",nsmgr) != null)
                ShamProbability = float.Parse(doc.DocumentElement.SelectSingleNode("//mini:GameDefinition/mini:ShamProbability", nsmgr).InnerText);
            if(doc.DocumentElement.SelectSingleNode("//mini:GameDefinition/mini:PlacingNewPiecesFrequency", nsmgr) != null)
                PlacingNewPiecesFrequency = Int32.Parse(doc.DocumentElement.SelectSingleNode("//mini:GameDefinition/mini:PlacingNewPiecesFrequency", nsmgr).InnerText);
            if(doc.DocumentElement.SelectSingleNode("//mini:GameDefinition/mini:InitialNumberOfPieces", nsmgr) != null)
                InitialNumberOfPieces = Int32.Parse(doc.DocumentElement.SelectSingleNode("//mini:GameDefinition/mini:InitialNumberOfPieces", nsmgr).InnerText);
            if(doc.DocumentElement.SelectSingleNode("//mini:GameDefinition/mini:BoardWidth", nsmgr) !=null)
                BoardWidth = Int32.Parse(doc.DocumentElement.SelectSingleNode("//mini:GameDefinition/mini:BoardWidth", nsmgr).InnerText);
            if(doc.DocumentElement.SelectSingleNode("//mini:GameDefinition/mini:TaskAreaLength", nsmgr) !=null)
                TaskAreaLength = Int32.Parse(doc.DocumentElement.SelectSingleNode("//mini:GameDefinition/mini:TaskAreaLength", nsmgr).InnerText);
            if(doc.DocumentElement.SelectSingleNode("//mini:GameDefinition/mini:GoalAreaLength", nsmgr) !=null)
                GoalAreaLength = Int32.Parse(doc.DocumentElement.SelectSingleNode("//mini:GameDefinition/mini:GoalAreaLength", nsmgr).InnerText);
            if(doc.DocumentElement.SelectSingleNode("//mini:GameDefinition/mini:NumberOfPlayersPerTeam", nsmgr) !=null)
                NumberOfPlayersPerTeam = Int32.Parse(doc.DocumentElement.SelectSingleNode("//mini:GameDefinition/mini:NumberOfPlayersPerTeam", nsmgr).InnerText);
            if(doc.DocumentElement.SelectSingleNode("//mini:GameDefinition/mini:GameName", nsmgr) !=null)
                GameName = doc.DocumentElement.SelectSingleNode("//mini:GameDefinition/mini:GameName", nsmgr).InnerText;
            if(doc.DocumentElement.SelectSingleNode("//mini:ActionCosts/mini:MoveDelay", nsmgr) !=null)
                MoveDelay = Int32.Parse(doc.DocumentElement.SelectSingleNode("//mini:ActionCosts/mini:MoveDelay", nsmgr).InnerText);
            if(doc.DocumentElement.SelectSingleNode("//mini:ActionCosts/mini:DiscoverDelay", nsmgr) !=null)
                DiscoverDelay = Int32.Parse(doc.DocumentElement.SelectSingleNode("//mini:ActionCosts/mini:DiscoverDelay", nsmgr).InnerText);
            if(doc.DocumentElement.SelectSingleNode("//mini:ActionCosts/mini:TestDelay", nsmgr) !=null)
                TestDelay = Int32.Parse(doc.DocumentElement.SelectSingleNode("//mini:ActionCosts/mini:TestDelay", nsmgr).InnerText);
            if(doc.DocumentElement.SelectSingleNode("//mini:ActionCosts/mini:PickUpDelay", nsmgr) !=null)
                PickUpDelay = Int32.Parse(doc.DocumentElement.SelectSingleNode("//mini:ActionCosts/mini:PickUpDelay", nsmgr).InnerText);
            if(doc.DocumentElement.SelectSingleNode("//mini:ActionCosts/mini:PlacingDelay", nsmgr) !=null)
                PlacingDelay = Int32.Parse(doc.DocumentElement.SelectSingleNode("//mini:ActionCosts/mini:PlacingDelay", nsmgr).InnerText);
            if(doc.DocumentElement.SelectSingleNode("//mini:ActionCosts/mini:KnowledgeExchangeDelay", nsmgr) !=null)
                KnowledgeExchangeDelay = Int32.Parse(doc.DocumentElement.SelectSingleNode("//mini:ActionCosts/mini:KnowledgeExchangeDelay", nsmgr).InnerText);
        }
    }
}
